package com.itheima.daggerapplication.di;

import android.app.Application;

import com.itheima.daggerapplication.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {ApplicationModule.class, ActivityBindingModule.class, AndroidSupportInjectionModule.class, CommonModule.class})
public interface AppComponent extends AndroidInjector<App> {



    @Component.Builder
    interface Builder {
//        传入的参数可以这里设置

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
