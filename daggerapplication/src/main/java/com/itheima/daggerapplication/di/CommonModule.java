package com.itheima.daggerapplication.di;

import android.app.Activity;

import com.itheima.daggerapplication.utils.ImageManager;
import com.itheima.daggerapplication.utils.LifecycleCallbackManager;

import java.util.Stack;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CommonModule {

    @Provides
    @Singleton
    public Stack<Activity> provideActivity(){
        return new Stack<Activity>();
    }


    @Provides
    @Singleton
    public ImageManager provideImageLoader() {
        return new ImageManager();
    }


    @Provides
    @Singleton
    public LifecycleCallbackManager provideLifecycleCallbackManager(){
        return new LifecycleCallbackManager();
    }

}
