package com.itheima.daggerapplication.di;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ApplicationModule {


    /**
     * 需要在appComponent里产生实例否则报错
     *
     * @param application
     * @return
     */
    @Binds
    abstract Context bindContext(Application application);


}

