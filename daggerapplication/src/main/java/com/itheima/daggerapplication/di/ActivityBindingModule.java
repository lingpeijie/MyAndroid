package com.itheima.daggerapplication.di;

import com.itheima.daggerapplication.activity.main.MainActivity;
import com.itheima.daggerapplication.activity.main.MainModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 *
 * 把application module的对象绑定到需要的activity中
 */
@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = AddEditTaskModule.class)
//    abstract AddEditTaskActivity addEditTaskActivity();
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = StatisticsModule.class)
//    abstract StatisticsActivity statisticsActivity();
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = TaskDetailPresenterModule.class)
//    abstract TaskDetailActivity taskDetailActivity();
}
