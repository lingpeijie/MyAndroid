package com.itheima.daggerapplication.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * 图片加载类
 */
public class ImageManager {


    public void loadNetUrl(ImageView imageView, String url, Context context) {
        Glide.with(context).load(url).into(imageView);
    }
}
