package com.itheima.daggerapplication;


import com.itheima.daggerapplication.di.DaggerAppComponent;
import com.itheima.daggerapplication.utils.LifecycleCallbackManager;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {

    @Inject
    LifecycleCallbackManager lifecycleCallbackManager;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new LifecycleCallbackManager());


    }
}
