package com.itheima.daggerapplication.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.DaggerActivity;

public abstract class BaseActivity extends DaggerActivity {
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        unbinder = ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder = null;
        }
    }

    /**
     * 获取布局id
     *
     * @return
     */
    public abstract int getLayout();

    /**
     * 初始化
     */
    public abstract void init();


}
