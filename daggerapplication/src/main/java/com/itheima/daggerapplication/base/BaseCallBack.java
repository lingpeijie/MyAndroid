package com.itheima.daggerapplication.base;

public interface BaseCallBack {

    void onSucess();

    void onError();
}
