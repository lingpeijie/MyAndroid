package com.itheima.daggerapplication.activity.main;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @Binds
    public abstract ContractMain.Presenter bingMainPresenter(PresenterMain mainActivityPresenter);


}
