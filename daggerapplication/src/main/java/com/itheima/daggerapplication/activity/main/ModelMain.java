package com.itheima.daggerapplication.activity.main;

import com.itheima.daggerapplication.base.BaseCallBack;
import com.itheima.daggerapplication.bean.User;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ModelMain implements ContractMain.Model {

    @Inject
    public ModelMain() {
    }

    @Override
    public void requestHomeData(User user, final BaseCallBack callBack) {


        Observable.just(user).observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<User>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(User user) {

                callBack.onSucess();


            }

            @Override
            public void onError(Throwable e) {
                callBack.onError();

            }

            @Override
            public void onComplete() {

            }
        });

    }
}
