package com.itheima.daggerapplication.activity.main;

import com.itheima.daggerapplication.base.BaseCallBack;
import com.itheima.daggerapplication.bean.User;

import javax.inject.Inject;

public class PresenterMain implements ContractMain.Presenter, BaseCallBack {
    @Inject
    MainActivity view;
    @Inject
    ModelMain model;
    @Inject
    public PresenterMain() {

    }


    @Override
    public void show() {
        User user = new User();
        user.setAge(18);
        user.setName("乔丹");
        model.requestHomeData(user, this);

    }

    @Override
    public void onSucess() {
        System.out.println("=======onSucess");

    }

    @Override
    public void onError() {
        System.out.println("=======onError");

    }
}
