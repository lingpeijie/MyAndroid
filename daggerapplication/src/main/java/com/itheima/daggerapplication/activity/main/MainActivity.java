package com.itheima.daggerapplication.activity.main;

import android.widget.ImageView;

import com.itheima.daggerapplication.R;
import com.itheima.daggerapplication.base.BaseActivity;
import com.itheima.daggerapplication.utils.ImageManager;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActivity {
    @Inject
    PresenterMain presenter;
    @Inject
    ImageManager imageManager;
    @BindView(R.id.imageView)
    ImageView imageView;

    private String url = "http://img4.imgtn.bdimg.com/it/u=2348781574,1710157032&fm=27&gp=0.jpg";


    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }


    @Override
    public void init() {
        imageManager.loadNetUrl(imageView, url, this);

    }

}
