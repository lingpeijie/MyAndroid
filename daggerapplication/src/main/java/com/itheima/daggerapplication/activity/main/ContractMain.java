package com.itheima.daggerapplication.activity.main;

import com.itheima.daggerapplication.base.BaseCallBack;
import com.itheima.daggerapplication.bean.User;

public interface ContractMain {

    interface Model {
        /**
         * 获取主页数据
         */
        void requestHomeData(User user, BaseCallBack callBack);
    }

    interface View {

    }

    interface Presenter {
        void show();

    }
}
