package com.itheima.myapplication.mvp.main;

import android.content.Context;
import android.os.Bundle;

import com.itheima.myapplication.R;

import javax.inject.Inject;

import dagger.android.DaggerActivity;

public class MainActivity extends DaggerActivity {


    @Inject
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.out.println("=============onCreateContextcom"+context);

    }
}
