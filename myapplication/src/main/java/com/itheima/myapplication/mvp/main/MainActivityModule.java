package com.itheima.myapplication.mvp.main;


import com.itheima.myapplication.mvp.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainActivityModule {


    /**
     * 绑定presenter
     *
     * @param presenter
     * @return
     */
    @ActivityScoped
    @Binds abstract MainContract.Presenter mainPresenter(MainPresenter presenter);


}
