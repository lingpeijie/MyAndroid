package com.itheima.myapplication.mvp.test.bean;

import javax.inject.Inject;

public class StudentBean {
    private int no;
    private String name;


    @Inject
    public StudentBean() {
        this.no = 1;
        this.name = "赵四";
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
