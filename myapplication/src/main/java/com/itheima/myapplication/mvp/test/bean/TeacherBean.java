package com.itheima.myapplication.mvp.test.bean;

import javax.inject.Inject;

public class TeacherBean {
    private String name;


    @Inject
    public TeacherBean() {
        this.name = "小强";
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
