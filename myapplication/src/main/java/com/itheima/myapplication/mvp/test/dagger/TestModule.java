package com.itheima.myapplication.mvp.test.dagger;

import com.itheima.myapplication.mvp.test.bean.StudentBean;

import dagger.Module;
import dagger.Provides;

@Module
public class TestModule {
    @Provides
    public StudentBean provideStudentBean() {
        return new StudentBean();
    }
}
