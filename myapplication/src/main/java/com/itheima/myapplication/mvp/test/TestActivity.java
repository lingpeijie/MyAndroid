package com.itheima.myapplication.mvp.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.itheima.myapplication.R;
import com.itheima.myapplication.mvp.dagger.DaggerAppComponent;
import com.itheima.myapplication.mvp.test.bean.StudentBean;
import com.itheima.myapplication.mvp.test.bean.TeacherBean;
import com.itheima.myapplication.mvp.test.dagger.DaggerTestActivityComponent;
import com.itheima.myapplication.mvp.test.dagger.TestModule;

import javax.inject.Inject;

public class TestActivity extends AppCompatActivity {

    @Inject
    StudentBean studentBean;

    @Inject
    TeacherBean teacherBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaggerTestActivityComponent.builder().build().inject(this);
        System.out.println("=====studentBean====="+studentBean.getName());
        System.out.println("======teacherBean===="+teacherBean.getName());

    }
}
