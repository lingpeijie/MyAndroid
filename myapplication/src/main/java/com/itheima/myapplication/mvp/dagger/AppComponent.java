package com.itheima.myapplication.mvp.dagger;

import android.app.Application;
import android.content.Context;

import com.itheima.MyApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {ApplicationModule.class, ActivityBindingModule.class, AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<MyApp> {

    @Component.Builder
    interface Builder {


//        相当于以前的module创建对象
        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
