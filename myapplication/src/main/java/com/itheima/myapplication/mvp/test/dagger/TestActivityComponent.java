package com.itheima.myapplication.mvp.test.dagger;

import com.itheima.myapplication.mvp.test.TestActivity;

import dagger.Component;

@Component()
public interface TestActivityComponent {
    void inject(TestActivity activity);
}
