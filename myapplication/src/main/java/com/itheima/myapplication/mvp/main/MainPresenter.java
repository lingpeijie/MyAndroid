package com.itheima.myapplication.mvp.main;

import com.itheima.myapplication.mvp.dagger.ActivityScoped;

@ActivityScoped
public class MainPresenter implements MainContract.Presenter{

//    private  MainRepository mainRepository;
//
//    @Inject
//    public MainPresenter(MainRepository mainRepository) {
//        this.mainRepository = mainRepository;
//    }

    @Override
    public void showMsg() {
        System.out.println("=====================showMsg");

    }
}
