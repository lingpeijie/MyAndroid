package com.itheima;


import android.content.Context;

import com.itheima.myapplication.mvp.dagger.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class MyApp extends DaggerApplication {
    @Inject
    Context context;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
//        运行在oncreate之前
        System.out.println("================AndroidInjector");
        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("=============onCreate");
        System.out.println("=============onCreateContext"+context);
    }
}
