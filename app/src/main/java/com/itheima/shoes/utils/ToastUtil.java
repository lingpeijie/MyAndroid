package com.itheima.shoes.utils;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;


public class ToastUtil {
    private Context mContext;
    public ToastUtil(Context mContext) {
        this.mContext = mContext;
    }

    public void showShortToast(String content){
        Toast.makeText(mContext,content,Toast.LENGTH_SHORT).show();
    }


    public void showLongToast(String content){
        Toast.makeText(mContext,content,Toast.LENGTH_LONG).show();
    }

    public void showSuccessToast(){
        Toast.makeText(mContext,"成功",Toast.LENGTH_LONG).show();
    }

    public void showErrorToast(){
        Toast.makeText(mContext,"失败",Toast.LENGTH_LONG).show();
    }


    public void printMessage(Object object){
        System.out.println("=============="+new Gson().toJson(object));
    }
}
