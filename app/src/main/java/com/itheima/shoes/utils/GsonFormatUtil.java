package com.itheima.shoes.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.Primitives;

import java.lang.reflect.Type;

/**
 * Created by lemon on 2017/10/10.
 */

public class GsonFormatUtil {
    private final Gson gson;

    public GsonFormatUtil() {
        gson = new GsonBuilder().create();
    }

    public String toJson(Object object) {
        String s = gson.toJson(object);
        return s;
    }

    public <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
        Object object = gson.fromJson(json,classOfT);
        return Primitives.wrap(classOfT).cast(object);
    }

//    public <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
//        Object object = fromJson(json, (Type) classOfT);
//        return Primitives.wrap(classOfT).cast(object);
//    }


}
