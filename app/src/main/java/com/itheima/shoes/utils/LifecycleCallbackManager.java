package com.itheima.shoes.utils;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;


import com.orhanobut.logger.Logger;

import java.util.Stack;


/**
 * Created by lemon on 2017/10/10.
 *
 * 统一管理生命周期
 *
 */

public class LifecycleCallbackManager implements Application.ActivityLifecycleCallbacks {

    private Stack<Activity> store;

    public LifecycleCallbackManager(Stack<Activity> store) {
        this.store = store;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        store.add(activity);
        Logger.i("onActivityCreated==========activity被创建："+activity.getClass().getName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Logger.i("onActivityStarted==========activity 开始");

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Logger.i("onActivityResumed==========activity 重新回到页面");

    }

    @Override
    public void onActivityPaused(Activity activity) {
        Logger.i("onActivityPaused==========activity暂停");

    }

    @Override
    public void onActivityStopped(Activity activity) {
        Logger.i("onActivityStopped==========activity停止");

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Logger.i("onActivitySaveInstanceState==========activity重新启动");

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Logger.i("onActivityDestroyed==========activity被销毁");
        store.remove(activity);
    }
}
