package com.itheima.shoes.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ImageLoaderUtil {
    private Context mContext;

    public ImageLoaderUtil(Context context) {
        this.mContext = context;
    }

    public void loadNetUrl(ImageView imageView, String url) {
        Glide.with(mContext).load(url).into(imageView);
    }
    public void loadNetUrlWithActivity(Activity activity, ImageView imageView, String url) {
        Glide.with(activity).load(url).into(imageView);
    }

    public void loadNetUrlWithFragment(Fragment fragment, ImageView imageView, String url) {
        Glide.with(fragment).load(url).into(imageView);
    }


}
