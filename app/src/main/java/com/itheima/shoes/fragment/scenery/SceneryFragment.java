package com.itheima.shoes.fragment.scenery;

import android.os.Bundle;

import com.itheima.shoes.R;
import com.itheima.shoes.base.BaseFragment;

public class SceneryFragment extends BaseFragment {
    public static SceneryFragment newInstance() {

        Bundle args = new Bundle();
        
        SceneryFragment fragment = new SceneryFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public int getLayoutId() {
        return R.layout.fragment_scenery;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initDate() {

    }
}
