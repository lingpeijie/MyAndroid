package com.itheima.shoes.fragment.deliciousfood;

import android.os.Bundle;

import com.itheima.shoes.R;
import com.itheima.shoes.base.BaseFragment;

public class DeliciousFoodFragment extends BaseFragment {
    public static DeliciousFoodFragment newInstance() {
        
        Bundle args = new Bundle();
        
        DeliciousFoodFragment fragment = new DeliciousFoodFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public int getLayoutId() {
        return R.layout.fragment_delicious_food;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initDate() {

    }

}
