package com.itheima.shoes.fragment.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.itheima.shoes.R;
import com.itheima.shoes.base.BaseFragment;
import com.itheima.shoes.fragment.deliciousfood.DeliciousFoodFragment;
import com.itheima.shoes.fragment.home.HomeFragment;
import com.itheima.shoes.fragment.scenery.SceneryFragment;
import com.itheima.shoes.fragment.user.UserFragment;
import com.itheima.shoes.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends BaseFragment {
    @BindView(R.id.homeBtn)
    Button homeBtn;
    @BindView(R.id.deliciousFoodBtn)
    Button deliciousFoodBtn;
    @BindView(R.id.sceneryBtn)
    Button sceneryBtn;
    @BindView(R.id.UserBtn)
    Button UserBtn;
    Unbinder unbinder;

    private HomeFragment homeFragment = null;
    private DeliciousFoodFragment deliciousFoodFragment = null;
    private SceneryFragment sceneryFragment = null;
    private UserFragment userFragment = null;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void initView() {
//        主页fragment
        homeFragment = (HomeFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mainFragmentContainer);
        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getActivity().getSupportFragmentManager(), homeFragment, R.id.mainFragmentContainer);
        }

    }

    @Override
    public void initDate() {

    }

    @OnClick({R.id.homeBtn, R.id.deliciousFoodBtn, R.id.sceneryBtn, R.id.UserBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.homeBtn:
                getActivity().getSupportFragmentManager().findFragmentById(R.id.mainFragmentContainer);

                if (homeFragment == null) {
                    homeFragment = HomeFragment.newInstance();
                }
                ActivityUtils.replaceFragment(getActivity().getSupportFragmentManager(), homeFragment, R.id.mainFragmentContainer);
                break;
            case R.id.deliciousFoodBtn:
                if (deliciousFoodFragment == null) {
                    deliciousFoodFragment = DeliciousFoodFragment.newInstance();
                }
                ActivityUtils.replaceFragment(getActivity().getSupportFragmentManager(), deliciousFoodFragment, R.id.mainFragmentContainer);
                break;
            case R.id.sceneryBtn:
                if (sceneryFragment == null) {
                    sceneryFragment = SceneryFragment.newInstance();
                }
                ActivityUtils.replaceFragment(getActivity().getSupportFragmentManager(), sceneryFragment, R.id.mainFragmentContainer);
                break;
            case R.id.UserBtn:
                if (userFragment == null) {
                    userFragment = UserFragment.newInstance();
                }
                ActivityUtils.replaceFragment(getActivity().getSupportFragmentManager(), userFragment, R.id.mainFragmentContainer);
                break;
        }
    }
}
