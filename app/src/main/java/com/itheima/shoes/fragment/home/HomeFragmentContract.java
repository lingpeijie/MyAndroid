package com.itheima.shoes.fragment.home;

public interface HomeFragmentContract {
    interface View{
        /**
         * 初始化轮播图
         */
        void initViewPager();
    }
}
