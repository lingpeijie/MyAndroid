package com.itheima.shoes.fragment.home;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.common.collect.Lists;
import com.itheima.shoes.R;
import com.itheima.shoes.adaper.BannerAdapter;
import com.itheima.shoes.adaper.HouseAdapter;
import com.itheima.shoes.base.BaseFragment;
import com.itheima.shoes.bean.User;

import java.util.ArrayList;
import java.util.LinkedList;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class HomeFragment extends BaseFragment implements OnRefreshListener, OnLoadMoreListener, HomeFragmentContract.View {
    @BindView(R.id.list)
    LRecyclerView list;
    /**
     * 当前加载的页数 默认是0 = 第一页
     */
    private int page = 0;
    private HouseAdapter houseAdapter;
    private LinkedList<User> linkedList;
    private String url = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531284640885&di=52db99a066a1cdb74329f8d20471f01b&imgtype=0&src=http%3A%2F%2Fzd20170706.oss-cn-shanghai.aliyuncs.com%2Fzh%2Fimages%2Fv2-f4dfe2d7e0d40bbd5a71d5fb737aad7c_b.jpg";
    private LRecyclerViewAdapter listAdapter;
    /**
     * list每次刷新显示的个数目
     */
    private int REQUEST_COUNT = 10;
    private ViewPager viewPager;
    private BannerAdapter bannerAdapter;
    private ArrayList<View> pageAdaperView = new ArrayList<>();
    private View headerView;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initView() {
        initViewPager();
        list = mView.findViewById(R.id.list);
        /**
         * 设置recycleView的布局排版方式
         */
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(layoutManager);
//        list.setPullRefreshEnabled(false);
        list.setOnRefreshListener(this);
        list.setOnLoadMoreListener(this);
        houseAdapter = new HouseAdapter(getContext());
        listAdapter = new LRecyclerViewAdapter(houseAdapter);
        listAdapter.addHeaderView(headerView);
        list.setAdapter(listAdapter);


    }

    @Override
    public void initDate() {
        onRefresh();

    }


    public LinkedList<User> builderUser() {
        LinkedList<User> linkedList = Lists.newLinkedList();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setAge(i);
            user.setName("新恒结衣" + i);
            user.setUrl(url);
            linkedList.addLast(user);
        }
        return linkedList;
    }

    @Override
    public void onRefresh() {
        page = 0;
        linkedList = builderUser();
        Observable.just(linkedList)
                .map(new Function<LinkedList<User>, LinkedList<User>>() {
                    @Override
                    public LinkedList<User> apply(LinkedList<User> users) throws Exception {
                        /**
                         * 模拟请求睡眠1.5秒
                         */
                        Thread.sleep(1500);
                        return users;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LinkedList<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LinkedList<User> users) {
                        linkedList = users;
                        houseAdapter.setDate(linkedList);
                        list.refreshComplete(REQUEST_COUNT);
                        listAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void onLoadMore() {
        /**
         * item是服务器返回的数据
         */
        LinkedList<User> item = builderUser();
        Observable.just(item)
                .map(new Function<LinkedList<User>, LinkedList<User>>() {
                    @Override
                    public LinkedList<User> apply(LinkedList<User> users) throws Exception {
//                        模拟服务器返回数据
                        Thread.sleep(1500);
                        return users;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LinkedList<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LinkedList<User> users) {
                        /**
                         * 设置数据并更新ui
                         */
                        linkedList.addAll(users);
                        list.refreshComplete(REQUEST_COUNT);
                        listAdapter.notifyDataSetChanged();
                        appComponent.getToastUril().showShortToast(page + "");
                        page++;
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    @Override
    public void initViewPager() {
        headerView = LayoutInflater.from(mContext).inflate(R.layout.header_view_banner, null);
        viewPager = headerView.findViewById(R.id.viewpage);
        bannerAdapter = new BannerAdapter();
        viewPager.setAdapter(bannerAdapter);
        for (int i = 0; i < 3; i++) {
            final View pager = LayoutInflater.from(mContext).inflate(R.layout.banner, null);
            pageAdaperView.add(pager);
        }
        bannerAdapter.setData(pageAdaperView);
        bannerAdapter.notifyDataSetChanged();
    }

}
