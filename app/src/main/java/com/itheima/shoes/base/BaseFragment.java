package com.itheima.shoes.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;

import com.itheima.shoes.App;
import com.itheima.shoes.dagger.AppComponent;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    protected View mView;
    protected Context mContext;
    protected AppComponent appComponent;
    protected Activity mActivity;
    private Unbinder bind;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(getLayoutId(), container, false);
            mContext = getContext();
            mActivity = getActivity();
            appComponent = App.getAppComponent();
            bind = ButterKnife.bind(this, mView);
            Logger.d("oncreateView");
        }
        initView();
        initDate();


        return mView;
    }

    /**
     * 获取布局id
     *
     * @return
     */
    public abstract int getLayoutId();

    /**
     * 初始化view
     */
    public abstract void initView();

    /**
     * 初始化数据
     */
    public abstract void initDate();

    protected void initToolBar(Toolbar toolbar,String title){
        toolbar.setTitle(title);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mView != null) {
            mView = null;
        }
        if (mContext != null) {
            mContext = null;
        }
        if (appComponent != null) {
            appComponent = null;
        }
        if (mActivity != null) {
            mActivity = null;
        }
        if (bind != null){
            bind = null;
        }
    }
}
