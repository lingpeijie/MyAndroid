package com.itheima.shoes.base;

public interface BaseContract {
    interface BaseView {

    }

    interface BasePresenter {

    }

    interface BaseModel {

    }
}
