package com.itheima.shoes.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.itheima.shoes.App;
import com.itheima.shoes.dagger.AppComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity<T extends BaseContract.BasePresenter> extends AppCompatActivity {
    protected AppComponent mAppComponent;
    private Unbinder bind = null;

    @Inject
    protected T mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        bind = ButterKnife.bind(this);
        mAppComponent = App.getAppComponent();
        initInject(mAppComponent);
        onInit();

    }

    /**
     * 获取布局id
     *
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * 初始化
     */
    protected abstract void onInit();

    /**
     * 初始化注入
     */
    protected abstract void initInject(AppComponent mAppComponent);

    protected void initToolBar(Toolbar toolbar) {
        toolbar.setTitle("gakki俱乐部");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null) {
            bind = null;
        }
    }
}
