package com.itheima.shoes.adaper;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.itheima.shoes.App;
import com.itheima.shoes.R;

import java.util.ArrayList;

public class BannerAdapter extends PagerAdapter {

    private ArrayList<View> pageAdaperViews;

    public void setData(ArrayList<View> pageAdaperViews) {
        this.pageAdaperViews = pageAdaperViews;
    }

    @Override
    public int getCount() {
        if (pageAdaperViews == null) {
            return 0;
        } else {
            return pageAdaperViews.size();
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
//        判断是否同一个
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final ImageView img = pageAdaperViews.get(position).findViewById(R.id.bannerImg);
        img.setImageResource(R.drawable.banner1);
        container.addView(pageAdaperViews.get(position));
        return pageAdaperViews.get(position);

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView(pageAdaperViews.get(position));
    }

}
