package com.itheima.shoes.adaper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.itheima.shoes.App;
import com.itheima.shoes.R;
import com.itheima.shoes.bean.User;

import java.util.LinkedList;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.ViewHolder> {
    private Context mContext;
    private LinkedList<User> linkedList;

    public HouseAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setDate(LinkedList<User> linkedList) {
        this.linkedList = linkedList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_house, viewGroup, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final User user = this.linkedList.get(i);
        App.getAppComponent().getImageLoader().loadNetUrl(viewHolder.houseImage, user.getUrl());

    }

    @Override
    public int getItemCount() {
        return linkedList != null ? linkedList.size() : 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView houseImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            houseImage = itemView.findViewById(R.id.houseImage);
        }
    }


}
