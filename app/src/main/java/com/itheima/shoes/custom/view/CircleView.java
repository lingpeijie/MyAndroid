package com.itheima.shoes.custom.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.itheima.shoes.R;

public class CircleView extends View implements View.OnTouchListener, GestureDetector.OnGestureListener {

    private final Paint mPaint;
    private final GestureDetector mGestureDetector;

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mGestureDetector = new GestureDetector(this);
        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setColor(getResources().getColor(R.color.themeColor));
        canvas.drawLine(0, 0, getWidth(), getHeight(), mPaint);
        canvas.drawLine(getWidth(), 0, 0, getHeight(), mPaint);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, 50, mPaint);
//        mPaint.setColor(getResources().getColor(R.color.white));
//        canvas.drawCircle(getWidth()/banner2,getHeight()/banner2,25,mPaint);
    }


    @Override
    public boolean onDown(MotionEvent motionEvent) {
        System.out.println("==========onDown");
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
        System.out.println("==========onShowPress");

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        System.out.println("==========onSingleTapUp");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        System.out.println("==========onScroll");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
        System.out.println("==========onLongPress");

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        System.out.println("==========onFling");
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        return mGestureDetector.onTouchEvent(motionEvent);
    }
}
