package com.itheima.shoes.activity.main;

import com.itheima.shoes.base.BaseContract;

public class MainActivityContract {

    interface Model extends BaseContract.BaseModel {
        /**
         * 测试显示数据
         */
        void showMsgFromService();

    }

    interface View extends BaseContract.BaseView {
        /**
         * 初始化fragmenr
         */

        void initFragmenr();

    }

    interface Presenter extends BaseContract.BasePresenter {

        /**
         * 向服务器请求数据
         */
        void requestData();



    }

}
