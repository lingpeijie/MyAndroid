package com.itheima.shoes.activity.main;

import android.support.v7.widget.Toolbar;

import com.itheima.shoes.R;
import com.itheima.shoes.base.BaseActivity;
import com.itheima.shoes.dagger.ActivityModule;
import com.itheima.shoes.dagger.AppComponent;
import com.itheima.shoes.dagger.DaggerActivityComponent;
import com.itheima.shoes.fragment.main.MainFragment;
import com.itheima.shoes.utils.ActivityUtils;
import com.itheima.shoes.utils.ToastUtil;

import javax.inject.Inject;

import butterknife.BindView;


public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityContract.View {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    ToastUtil toastUtil;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initInject(AppComponent appComponent) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(new ActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onInit() {
        initFragmenr();
        initToolBar(toolbar);
        mPresenter.requestData();

    }


    @Override
    public void initFragmenr() {
        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (mainFragment == null) {
            mainFragment = MainFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), mainFragment, R.id.fragmentContainer);
        }
    }

}
