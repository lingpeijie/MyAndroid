package com.itheima.shoes.activity.test;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.itheima.shoes.R;

import dagger.android.support.DaggerAppCompatActivity;

public class TestActivity extends DaggerAppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}
