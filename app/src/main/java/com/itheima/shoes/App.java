package com.itheima.shoes;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.itheima.shoes.dagger.AppComponent;
import com.itheima.shoes.dagger.AppModule;
import com.itheima.shoes.dagger.DaggerAppComponent;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

/**
 * @author lemon
 * test 我是开发版本 我是八块fsdf
 */
public class App extends Application {
    private static AppComponent appComponent;
    @Inject
    AndroidLogAdapter androidLogAdapter;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
        appComponent.inject(this);
        Logger.addLogAdapter(androidLogAdapter);
        AndroidNetworking.initialize(this);
    }
}
