package com.itheima.shoes.dagger;

import android.content.Context;

import com.itheima.shoes.utils.GsonFormatUtil;
import com.itheima.shoes.utils.ImageLoaderUtil;
import com.itheima.shoes.utils.ToastUtil;
import com.orhanobut.logger.AndroidLogAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context mContext) {
        this.mContext = mContext;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    public ImageLoaderUtil provideImageLoader(Context context) {
        return new ImageLoaderUtil(context);
    }

    @Provides
    @Singleton
    public ToastUtil provideToastUtil(Context context) {
        return new ToastUtil(context);
    }

    @Provides
    @Singleton
    public GsonFormatUtil provideGsonFormatUtil() {
        return new GsonFormatUtil();
    }

    @Provides
    @Singleton
    public AndroidLogAdapter provideAndroidLogAdapter() {
        return new AndroidLogAdapter();
    }


}
