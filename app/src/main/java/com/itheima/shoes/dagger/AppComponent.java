package com.itheima.shoes.dagger;

import android.content.Context;

import com.itheima.shoes.App;
import com.itheima.shoes.utils.GsonFormatUtil;
import com.itheima.shoes.utils.ImageLoaderUtil;
import com.itheima.shoes.utils.ToastUtil;
import com.orhanobut.logger.AndroidLogAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    Context context();

    ToastUtil getToastUril();

    ImageLoaderUtil getImageLoader();

    GsonFormatUtil getGson();

    AndroidLogAdapter getAndroidLogAdapter();

    void inject(App app);

}
