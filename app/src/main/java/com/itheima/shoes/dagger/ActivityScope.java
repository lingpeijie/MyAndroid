package com.itheima.shoes.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;


/**
 *
 * @author lemon
 * 自定义注解，对应Activity的生命周期，Dagger2可以通过自定义注解限定注解作用域。
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
