package com.itheima.shoes.dagger;

import android.app.Activity;

import com.itheima.shoes.activity.main.MainActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity activity();

    void inject(MainActivity mainActivity);
}
