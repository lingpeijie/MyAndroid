package com.itheima.shoes.bean;


import java.util.List;

public class Calendar {
    private List<Year> years;

    public List<Year> getYears() {
        return years;
    }

    public void setYears(List<Year> years) {
        this.years = years;
    }

    public static class Year {
        private int id;
        private List<Month> months;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<Month> getMonths() {
            return months;
        }

        public void setMonths(List<Month> months) {
            this.months = months;
        }
    }

    public static class Month {
        private int id;
        private List<Day> days;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<Day> getDays() {
            return days;
        }

        public void setDays(List<Day> days) {
            this.days = days;
        }
    }


    public static class Day {
        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
