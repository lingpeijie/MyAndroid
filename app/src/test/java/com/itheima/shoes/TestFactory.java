package com.itheima.shoes;


import android.content.Context;
import android.content.res.AssetManager;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestFactory {

    @Test
    public void myTestMethod() throws IOException {
        // 1. 读取属性配置文件

        Properties pro = new Properties();
        final Context context = App.getAppComponent().context();
        final AssetManager assets = context.getAssets();
        final InputStream open = assets.open("Product.properties");
        pro.load(open);

        // 2. 获取属性配置文件中的产品类名
        String Classname = pro.getProperty("ProductA");

        // 3. 动态生成产品类实例
        Product concreteProduct = TestFactory.getInstance(Classname);

        // 4. 调用该产品类对象的方法，从而生产产品
        concreteProduct.show();

    }


    // 定义方法：通过反射动态创建产品类实例
    public static Product getInstance(String ClassName) {
        Product concreteProduct = null;
        try {
            // 1. 根据 传入的产品类名 获取 产品类类型的Class对象
            Class product_Class = Class.forName(ClassName);
            // 2. 通过Class对象动态创建该产品类的实例
            concreteProduct = (Product) product_Class.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 3. 返回该产品类实例
        return concreteProduct;
    }


}

