package com.itheima.shoes;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MapTest {

    @Test
    public void showMsg() {
//        HashMap<Integer, User> objectObjectHashMap = Maps.newHashMap();
//        objectObjectHashMap.put(banner1,new User());
//        objectObjectHashMap.get(banner1);
//        objectObjectHashMap.keySet();
        int a = 1;
        int b = 3;
        int result = a + b;
        

//        Assert.assertEquals(result, 4);
//        System.out.println("test");
    }

    @Test
    public void counter(){
        int studentNum = 50;
        String sout = studentNum > 100 || studentNum < 0 ? "无效" : studentNum >= 90 ? "a级别" : studentNum<90 && studentNum>=60?"b级别":"c级别";
        System.out.println(sout);
    }

    @Test
    public void testProduct(){
        // 1. 获取Student类的Class对象
        Class studentClass = Student.class;

        // 2. 通过Class对象创建Student类的对象
        Object  mStudent = null;
        try {
            mStudent = studentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // 3.1 通过Class对象获取方法setName1（）的Method对象:需传入方法名
        // 因为该方法 = 无参，所以不需要传入参数
        Method msetName1 = null;
        try {
            msetName1 = studentClass.getMethod("setName1");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // 通过Method对象调用setName1（）：需传入创建的实例
        try {
            msetName1.invoke(mStudent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        // 3.2 通过Class对象获取方法setName2（）的Method对象:需传入方法名 & 参数类型
        Method msetName2 = null;
        try {
            msetName2 = studentClass.getMethod("setName2",String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // 通过Method对象调用setName2（）：需传入创建的实例 & 参数值
        try {
            msetName2.invoke(mStudent,"Carson_Ho");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

}
