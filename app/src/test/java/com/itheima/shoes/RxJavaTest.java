package com.itheima.shoes;


import com.orhanobut.logger.Logger;

import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.operators.observable.ObservableJust;
import io.reactivex.schedulers.Schedulers;

public class RxJavaTest {

    @Test
    public void showMsg(){
        Observable.just(213)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        System.out.println(d);

                    }

                    @Override
                    public void onNext(Integer integer) {
                        System.out.println(integer);

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e);

                    }

                    @Override
                    public void onComplete() {
                        System.out.println("complete");

                    }
                });


    }



    @Test
    public void netWorking(){


        Observable.just("hahahah")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println("accept:"+s);

                    }
                })
                .observeOn(Schedulers.io())
                .flatMap(new Function<String, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(String s) throws Exception {
                        System.out.println("ObservableSource:"+s);
                        return Observable.just(s);
                    }
                })
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Object o) {
                        System.out.println("============"+o.toString());

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



    @Test
    public void reduce(){
//        具有合并减少作用
        Observable.just(1,2,3,4,5,6,7,8,9,10)
                .reduce(new BiFunction<Integer, Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer, Integer integer2) throws Exception {
                        System.out.println("intefer:"+integer);
                        System.out.println("intefer2:"+integer2);
                        return integer * integer2;
                    }
                })
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        System.out.println(integer);
                    }
                });


    }



    @Test
    public void testMerge() {
        int a = 1 ;
        int b = 2 ;
        int c = 3 ;
        int d = 4 ;
        int e = 5 ;
        int f = 6 ;


        Observable.merge(Observable.just(a, b, c), Observable.just(d, e, f)).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                System.out.println("=================" + integer);
            }
        });
    }


    @Test
    public void testLast() {
//        TimeUtil.getNowStrTime();

        Observable.just(1, 2, 3);
        Observable.just(4, 5, 6);

        Observable.just(1, 10, -1)
                .last(2)
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        Logger.e("last:" + integer);
                        System.out.println("last:" + integer);

                    }
                });

    }
}
